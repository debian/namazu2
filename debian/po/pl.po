# Copyright (C) 2012
# This file is distributed under the same license as the namazu2 package.
#
# Michał Kułach <michal.kulach@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: namazu2\n"
"Report-Msgid-Bugs-To: namazu2@packages.debian.org\n"
"POT-Creation-Date: 2012-01-28 07:52+0100\n"
"PO-Revision-Date: 2012-01-30 14:23+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <debian-l10n-polish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Directories in which to copy the cgi:"
msgstr "Katalogi do skopiowania cgi:"

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"Namazu package will be installed in /usr/lib/cgi-bin/namazu.cgi by default. "
"But for VirtualHost, you may also need copy the cgi scripts to another "
"location. The cgi will be copied automatically on upgrade or installation."
msgstr ""
"Pakiet Namazu zostanie zainstalowany domyślnie w /usr/lib/cgi-bin/namazu."
"cgi. W przypadku wykorzystywania VirtualHosta może również zajść potrzeba "
"skopiowania skryptów cgi do innego położenia. Będą one kopiowane "
"automatycznie przy aktualizacji lub instalacji."

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"Directories should be space separated. If you don't need this feature, "
"please leave this option to the empty string."
msgstr ""
"Katalogi powinny być oddzielone spacją. Aby nie korzystać z tej funkcji, "
"proszę pozostawić pole puste."
